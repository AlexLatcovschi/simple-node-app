const Mongoose = require('mongoose');

module.exports = Mongoose.model("product", Mongoose.Schema(
    {
        title: [{type: Object}],
        description: [{type: Object}],
        mainDescription: [{type: Object}],
        slug: {type: String},
        order: {type: String},
        accessCount: {type: Number},
        price: {type: String},
        category: {type: Object},
        company: {type: Object},
        image: {
            description: String,
            path: {type: String, default: null},
            mimetype: {type: String, default: null},
            updatedAt: {type: Date, default: Date.now},
        },
    }
));
