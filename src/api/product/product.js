const Boom = require('boom');
const ProductModel = require('./models/product.model.js');

module.exports = [
    {
        method: 'GET',
        path: '/products',
        config: {
            async handler() {
                try {
                    return ProductModel.find().lean(true);
                }
                catch (err) {
                    throw Boom.internal('Internal MongoDB error', err);
                }
            }
        },
    },
    {
        method: 'GET',
        path: '/products/{slug}',
        config: {
            async handler(request) {
                try {
                    return ProductModel.findOne({slug: request.params.slug});
                }
                catch (err) {
                    throw Boom.internal('Internal MongoDB error', err);
                }
            }
        },
    }
];
