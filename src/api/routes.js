const fs = require('fs');
let routes = [];

fs.readdirSync(__dirname)
    .forEach(folder => {
        if(!folder.includes('.js')) {
            fs.readdirSync(`${__dirname}\\${folder}`)
                .forEach(file => {
                    if(file.includes('.js')) {
                        routes = routes.concat(require(`./${folder}/${file}`))
                    }
                })
        }
    });

module.exports = routes;

