const Boom = require('boom');
const https = require('https');
const ProductModel = require('../product/models/product.model.js');
const ProductCategoryModel = require('../product-category/models/product-category.model.js');

module.exports = [
    {
        method: 'GET',
        path: '/seed',
        config: {
            async handler(request) {
                try {
                    // data from one of my projects
                    https.get('https://api.coffeein.md/api/products/sorted?page=1&searchTerm=&productCategory=&company=coffeein', (resp) => {
                        let data = '';

                        // A chunk of data has been received.
                        resp.on('data', (chunk) => {
                            data += chunk;
                        });

                        // The whole response has been received. Print out the result.
                        resp.on('end', async () => {
                            const parsedData = [];
                            JSON.parse(data).products.map(el => {
                                parsedData.push(...el.products);
                            });

                            ProductModel.deleteMany({}).then(() => {
                                parsedData.map(async el => {
                                    await ProductModel.create(el)
                                })
                            })
                        });

                    }).on("error", (err) => {
                        console.log("Error: " + err.message);
                    });

                    https.get('https://api.coffeein.md/api/categories', (resp) => {
                        let data = '';

                        // A chunk of data has been received.
                        resp.on('data', (chunk) => {
                            data += chunk;
                        });

                        // The whole response has been received. Print out the result.
                        resp.on('end', async () => {
                            const categories = [];

                            JSON.parse(data).docs.map(el => {
                                categories.push(el);
                            });

                            ProductCategoryModel.deleteMany({}).then(() => {
                                categories.map(async el => {
                                    await ProductCategoryModel.create(el)
                                })
                            })
                        });
                    }).on("error", (err) => {
                        console.log("Error: " + err.message);
                    });
                    return 'Seed generated!';
                } catch (err) {
                    throw Boom.internal('Internal MongoDB error', err);
                }
            }
        },
    }
];
