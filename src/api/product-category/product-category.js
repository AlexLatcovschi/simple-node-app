const Boom = require('boom');
const ProductCategoryModel = require('./models/product-category.model.js');

module.exports = [
    {
        method: 'GET',
        path: '/product-categories',
        config: {
            async handler() {
                try {
                    return ProductCategoryModel.find().lean(true);
                } catch (err) {
                    throw Boom.internal('Internal MongoDB error', err);
                }
            }
        },
    },
    {
        method: 'POST',
        path: '/product-categories',
        config: {
            async handler(request) {
                try {
                    return ProductCategoryModel.create(request.payload);
                } catch (err) {
                    throw Boom.internal('Internal MongoDB error', err);
                }
            }
        },
    }
];
