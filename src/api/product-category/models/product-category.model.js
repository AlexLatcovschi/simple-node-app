const Mongoose = require('mongoose');

module.exports = Mongoose.model("productCategory", Mongoose.Schema(
    {
        name: [{type: Object}],
        description: [{type: Object}],
        slug: {type: String},
        range: {type: String},
    }
));
