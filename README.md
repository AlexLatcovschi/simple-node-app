# simple-node-app

## Use nodemon to serve the project
- access http://localhost:3000/seed to generate some data
- access http://localhost:3000/companies to see fetched companies
- access http://localhost:3000/companies/id to see fetched company by id
- access http://localhost:3000/product-category to see fetched product categories
- post http://localhost:3000/product-category to add new product category 
-> obj example -> {"name":[{"shortName":"ro","content":"Bauturi calde"},{"shortName":"ru","content":"Теплые напитки"},{"shortName":"en","content":"Hot drinks"}],"description":[{"shortName":"ro","content":""},{"shortName":"ru","content":""},{"shortName":"en","content":""}],"slug":"hot-drinks","range":"3","__v":0}



