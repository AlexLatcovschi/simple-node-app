const Hapi = require('@hapi/hapi');
const Boom = require('boom');
const db = require('./src/database/database.js').db;

const routes = require('./src/api/routes.js');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
